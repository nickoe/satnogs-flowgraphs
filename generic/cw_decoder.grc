options:
  parameters:
    author: Manolis Surligas (surligas@gmail.com)
    category: Custom
    cmake_opt: ''
    comment: ''
    copyright: ''
    description: A CW (Morse) Decoder
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: no_gui
    hier_block_src_path: '.:'
    id: satnogs_cw_decoder
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: run
    sizing_mode: fixed
    thread_safe_setters: ''
    title: CW Decoder
    window_size: 3000, 3000
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 8]
    rotation: 0
    state: enabled

blocks:
- name: audio_samp_rate
  id: variable
  parameters:
    comment: ''
    value: '48000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1240, 12.0]
    rotation: 0
    state: enabled
- name: dot_samples
  id: variable
  parameters:
    comment: ''
    value: int((1.2 / wpm) / (1.0 / (audio_samp_rate / 10.0)))
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1360, 12.0]
    rotation: 0
    state: enabled
- name: variable_cw_decoder_0
  id: variable_cw_decoder
  parameters:
    channels: '4'
    comment: ''
    confidence: '0.90'
    fft_size: '512'
    max_frame_size: '96'
    min_frame_size: '8'
    overlapping: 512-8
    samp_rate: audio_samp_rate/4
    snr: '10'
    wpm: wpm
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1376, 220.0]
    rotation: 0
    state: true
- name: analog_agc2_xx_0_0
  id: analog_agc2_xx
  parameters:
    affinity: ''
    alias: ''
    attack_rate: '0.01'
    comment: ''
    decay_rate: '0.001'
    gain: '0.0'
    max_gain: '65536'
    maxoutbuf: '0'
    minoutbuf: '0'
    reference: '0.015'
    type: complex
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [616, 348.0]
    rotation: 0
    state: enabled
- name: antenna
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [880, 12.0]
    rotation: 0
    state: enabled
- name: bfo_freq
  id: parameter
  parameters:
    alias: ''
    comment: 'The bfo_freq shifts the carrier from 0 Hz to a

      frequency that corresponds to the CW audio

      tone. This tone is typically 1 kHz.'
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: 1e3
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1368, 76.0]
    rotation: 0
    state: enabled
- name: blocks_complex_to_real_0
  id: blocks_complex_to_real
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1000, 384.0]
    rotation: 0
    state: enabled
- name: blocks_rotator_cc_0_0
  id: blocks_rotator_cc
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    phase_inc: 2.0 * math.pi * (bfo_freq / audio_samp_rate)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [808, 380.0]
    rotation: 0
    state: enabled
- name: bw
  id: parameter
  parameters:
    alias: ''
    comment: 'The bandwidth should configure RF filters on some devices.

      Set to 0.0 for automatic calculation.'
    hide: none
    label: Bandwidth
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1040, 12.0]
    rotation: 0
    state: enabled
- name: decoded_data_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"/tmp/.satnogs/data/data"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [0, 748.0]
    rotation: 0
    state: enabled
- name: dev_args
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Device arguments
    short_id: ''
    type: str
    value: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [432, 12.0]
    rotation: 0
    state: enabled
- name: doppler_correction_per_sec
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '20'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [160, 748.0]
    rotation: 0
    state: enabled
- name: enable_iq_dump
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [256, 668.0]
    rotation: 0
    state: enabled
- name: file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"test.wav"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [144, 668.0]
    rotation: 180
    state: enabled
- name: gain
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [960, 12.0]
    rotation: 0
    state: enabled
- name: import_0
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import satnogs
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [200, 20.0]
    rotation: 0
    state: enabled
- name: import_1
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import math
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [200, 68.0]
    rotation: 0
    state: enabled
- name: iq_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"/tmp/iq.dat"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [400, 668.0]
    rotation: 0
    state: enabled
- name: lo_offset
  id: parameter
  parameters:
    alias: ''
    comment: 'To avoid the SDR carrier at the DC

      we shift the LO a little further'
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: 100e3
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [800, 12.0]
    rotation: 0
    state: enabled
- name: low_pass_filter_0_0
  id: low_pass_filter
  parameters:
    affinity: ''
    alias: ''
    beta: '6.76'
    comment: ''
    cutoff_freq: '3000'
    decim: '1'
    gain: '1'
    interp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: audio_samp_rate
    type: fir_filter_ccf
    width: 1e3
    win: firdes.WIN_HAMMING
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [432, 444.0]
    rotation: 0
    state: enabled
- name: rational_resampler_xxx_0
  id: rational_resampler_xxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decim: '4'
    fbw: '0'
    interp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    taps: ''
    type: ccc
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [616, 468.0]
    rotation: 0
    state: true
- name: rigctl_port
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '4532'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [400, 748.0]
    rotation: 0
    state: enabled
- name: rx_freq
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: 100e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [720, 12.0]
    rotation: 0
    state: enabled
- name: samp_rate_rx
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Device Sampling rate
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [568, 12.0]
    rotation: 0
    state: enabled
- name: satnogs_doppler_compensation_0
  id: satnogs_doppler_compensation
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    compensate: '1'
    fine_correction: '0'
    lo_offset: lo_offset
    maxoutbuf: '0'
    minoutbuf: '0'
    out_samp_rate: audio_samp_rate
    samp_rate: samp_rate_rx
    sat_freq: rx_freq
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [568, 132.0]
    rotation: 0
    state: true
- name: satnogs_frame_decoder_0
  id: satnogs_frame_decoder
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decoder_object: variable_cw_decoder_0
    itype: complex
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [800, 496.0]
    rotation: 0
    state: true
- name: satnogs_frame_file_sink_0_0
  id: satnogs_frame_file_sink
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    output_type: '0'
    prefix_name: decoded_data_file_path
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1208, 564.0]
    rotation: 0
    state: enabled
- name: satnogs_iq_sink_0
  id: satnogs_iq_sink
  parameters:
    activate: enable_iq_dump
    affinity: ''
    alias: ''
    append: 'False'
    comment: ''
    filename: iq_file_path
    scale: '16768'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [40, 548.0]
    rotation: 180
    state: enabled
- name: satnogs_json_converter_0
  id: satnogs_json_converter
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    extra: ''
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1016, 508.0]
    rotation: 0
    state: enabled
- name: satnogs_ogg_encoder_0
  id: satnogs_ogg_encoder
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    filename: file_path
    quality: '1.0'
    samp_rate: audio_samp_rate
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1144, 364.0]
    rotation: 0
    state: enabled
- name: satnogs_tcp_rigctl_msg_source_0
  id: satnogs_tcp_rigctl_msg_source
  parameters:
    addr: '"127.0.0.1"'
    affinity: ''
    alias: ''
    comment: ''
    interval: int(1000.0/doppler_correction_per_sec) + 1
    maxoutbuf: '0'
    minoutbuf: '0'
    mode: 'False'
    mtu: '1500'
    port: rigctl_port
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [304, 252.0]
    rotation: 0
    state: enabled
- name: satnogs_udp_msg_sink_0_0
  id: satnogs_udp_msg_sink
  parameters:
    addr: udp_IP
    affinity: ''
    alias: ''
    comment: ''
    mtu: '1500'
    port: udp_port
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1208, 476.0]
    rotation: 0
    state: enabled
- name: satnogs_waterfall_sink_0
  id: satnogs_waterfall_sink
  parameters:
    affinity: ''
    alias: ''
    center_freq: rx_freq
    comment: ''
    fft_size: '1024'
    filename: waterfall_file_path
    mode: '1'
    rps: '10'
    samp_rate: audio_samp_rate
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [40, 356.0]
    rotation: 180
    state: enabled
- name: soapy_rx_device
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"driver=invalid"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [304, 12.0]
    rotation: 0
    state: enabled
- name: soapy_source_0
  id: soapy_source
  parameters:
    affinity: ''
    alias: ''
    amp_gain0: '0'
    ant0: antenna
    ant1: RX2
    args: dev_args
    balance0: '0'
    balance1: '0'
    bw0: bw
    bw1: '0'
    center_freq0: rx_freq - lo_offset
    center_freq1: '0'
    clock_rate: '0'
    clock_source: ''
    comment: ''
    correction0: '0'
    correction1: '0'
    dc_offset0: '0'
    dc_offset1: '0'
    dc_offset_auto_mode0: 'False'
    dc_offset_auto_mode1: 'False'
    dev: soapy_rx_device
    devname: custom
    gain_auto_mode0: 'False'
    gain_auto_mode1: 'False'
    ifgr_gain: '59'
    lna_gain0: '10'
    lna_gain1: '10'
    manual_gain0: 'False'
    manual_gain1: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    mix_gain0: '10'
    mix_gain1: '10'
    nchan: '1'
    nco_freq0: '0'
    nco_freq1: '0'
    overall_gain0: gain
    overall_gain1: '10'
    pga_gain0: '24'
    pga_gain1: '24'
    rfgr_gain: '9'
    samp_rate: samp_rate_rx
    sdrplay_agc_setpoint: '-30'
    sdrplay_biastee: 'True'
    sdrplay_dabnotch: 'False'
    sdrplay_if_mode: Zero-IF
    sdrplay_rfnotch: 'False'
    tia_gain0: '0'
    tia_gain1: '0'
    tuner_gain0: '10'
    tuner_gain1: '10'
    type: fc32
    vga_gain0: '10'
    vga_gain1: '10'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [304, 116.0]
    rotation: 0
    state: true
- name: udp_IP
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"127.0.0.1"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [496, 668.0]
    rotation: 0
    state: enabled
- name: udp_port
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '16887'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [496, 748.0]
    rotation: 0
    state: enabled
- name: virtual_sink_0
  id: virtual_sink
  parameters:
    alias: ''
    comment: ''
    stream_id: doppler_corrected
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [864, 172.0]
    rotation: 0
    state: true
- name: virtual_source_0
  id: virtual_source
  parameters:
    alias: ''
    comment: ''
    stream_id: doppler_corrected
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [40, 492.0]
    rotation: 0
    state: true
- name: waterfall_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"/tmp/waterfall.dat"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [0, 668.0]
    rotation: 0
    state: enabled
- name: wpm
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '20'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1168, 12.0]
    rotation: 0
    state: enabled

connections:
- [analog_agc2_xx_0_0, '0', blocks_rotator_cc_0_0, '0']
- [blocks_complex_to_real_0, '0', satnogs_ogg_encoder_0, '0']
- [blocks_rotator_cc_0_0, '0', blocks_complex_to_real_0, '0']
- [low_pass_filter_0_0, '0', analog_agc2_xx_0_0, '0']
- [low_pass_filter_0_0, '0', rational_resampler_xxx_0, '0']
- [rational_resampler_xxx_0, '0', satnogs_frame_decoder_0, '0']
- [satnogs_doppler_compensation_0, '0', virtual_sink_0, '0']
- [satnogs_frame_decoder_0, out, satnogs_json_converter_0, in]
- [satnogs_json_converter_0, out, satnogs_frame_file_sink_0_0, frame]
- [satnogs_json_converter_0, out, satnogs_udp_msg_sink_0_0, in]
- [satnogs_tcp_rigctl_msg_source_0, freq, satnogs_doppler_compensation_0, doppler]
- [soapy_source_0, '0', satnogs_doppler_compensation_0, '0']
- [virtual_source_0, '0', low_pass_filter_0_0, '0']
- [virtual_source_0, '0', satnogs_iq_sink_0, '0']
- [virtual_source_0, '0', satnogs_waterfall_sink_0, '0']

metadata:
  file_format: 1
